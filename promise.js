// resolve
const prom = new Promise ((resolve, reject) => {
    // here is async
    setTimeout( () => {
        resolve({ user: "Ed", pass: "o2i43h24" });
    }, 2000);
});

prom.then( data => {
    console.log(data);
});



// rejected
const promise = new Promise ((resolve, reject) => {
    // here is async
    setTimeout( () => {
        reject( new Error("Something went wrong!"));
    }, 3000);
});

promise.then(data => {
    console.log(data);
})
.catch(err => console.log(err));
// let dan const

// let => dapat digunakan untuk mengubah isi / reasign
let counter = 5;
counter = 10;

console.log(counter); // output will be 10

let cnt = 5;
let cnt = 10;

console.log(cnt); // output will be error, has already been declared


// const => constanta atau nilai pasti dan tidak dapat diubah
const name = 'Dennis';
name = 'Ed';

console.log(name); // output will be error, assigmnet to constant variable.



// note : let dan const adalah block scope
const list = [1, 2, 3, 4, 5];
for( let i = 0; i < list.length; i++ ){
    console.log(i);
}

console.log(i); // let dan const tidak akan menjalankan ini / error! tapi jika i adalah var maka akan dijalankan.
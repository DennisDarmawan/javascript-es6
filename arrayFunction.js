const shoppingList = ["Milk", "Cow", "Eggs", "Bananas", "Choco"];


// forEach
shoppingList.forEach((product, index) => {
    console.log(`The index is ${index} and the product is ${product}`);
}); // will looping for each


// .map
const newList = shoppingList.map( item => item + " new" );
console.log(newList); // looping and give a new value


// .filter
const filterList = shoppingList.filter( item => item !== "Eggs" );
console.log(filterList);
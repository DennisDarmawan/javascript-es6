const add = ( c = 1, d = 2) => { // parameter dapat diberi nilai default
    console.log(c + d);
}

add(3, 5); // jika nilai default tidak ingin digunakan maka dapat memberi nilai tersendiri
const home = () => {
    console.log("My home is unknown");
};

home();


const sayLocation = location => console.log(`My location is ${location}`); // Jika satu line code maka dapat dilakukan seperti ini.

sayLocation("Paris");


const user = {
    name: "Dennis",
    age: 23,
    sayName: function(){
        console.log(`My name is ${this.name}`);
        const fullName = () => {
            console.log(`My name is ${this.name} and my age is ${this.age}`);
        };
        fullName();
    }
};

user.sayName();
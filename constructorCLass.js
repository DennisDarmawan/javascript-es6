class ShoppingList {
    constructor( items, nr ){
        this.items = items;
        this.nr = nr;
    }

    sayList(){
        console.log(this.items);
    }
}

const myList = new ShoppingList(['Milk, Choco, Redbull'], 3);

class Product extends ShoppingList{
    constructor(items, nr, amount, cost){
        super(items, nr);
        this.amount = amount;
        this.cost = cost;
    }
}

const product = new Product(['Redbull, Chocolate, Candy'], 3, 2, 20);

product.sayList();